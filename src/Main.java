import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws IOException {

        InputStream inp = new FileInputStream("src/sitemap.xlsx");

        Workbook workbook = WorkbookFactory.create(inp);
        Sheet sheet = workbook.getSheetAt(0);
        Row row = null;

        ArrayList<String> https = new ArrayList<>();

        for (int i = 0; i <= sheet.getLastRowNum(); i++) {
            row = sheet.getRow(i);
            https.add(row.getCell(0).getStringCellValue());
        }

        for (int i = 0; i < https.size(); i++) {

            Document document = Jsoup.connect(https.get(i)).ignoreHttpErrors(true).get();
            Elements elements = document.getElementsByTag("img");

            for (Element element :
                    elements) {

                String photoName =
                        element.attr("src").substring(
                                element.attr("src").lastIndexOf("/") + 1,
                                element.attr("src").lastIndexOf(".")
                        );

                String article = photoName.contains("_") ?
                        photoName.substring(1, photoName.lastIndexOf("_")) :
                        photoName;

                if (!element.attr("src").equals("/images/cartdeliv.png") &&
                        !element.attr("src").equals("/images/main-logo.png")) {

                    System.out.println(
                            "Строка Excel: " + i + " --- Артикул: " + article
                                    + " --- ссылка: https://artelamp.it" + element.attr("src")
                    );
                }

            }
        }

    }
}
